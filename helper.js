

var Field = require("./field");

var oppositeDir = 
{
    "left": "right",
    "right": "left",
    "up": "down",
    "down": "up"
};


function move(pos, dir)
{
    if(dir == "left")
    {
        return {col: pos.col - 1, row: pos.row};
    }
    else if(dir == "right")
    {
        return {col: pos.col + 1, row: pos.row};
    }
    else if(dir == "up")
    {
        return {col: pos.col, row: pos.row - 1};
    }
    else if(dir == "down")
    {
        return {col: pos.col, row: pos.row + 1};
    }
}



function isSamePosition(lhs, rhs)
{
    return lhs.row == rhs.row && lhs.col == rhs.col;
}



// TODO: document
function _changeField(field, position, change)
{
    var current = field.get(position.col, position.row);
    var newValue = change(current);

    field.set(position.col, position.row, newValue);
}


// TODO: document
function movePlayer(theField, from, to)
{
    _changeField(theField, from, Field.removePlayer);
    _changeField(theField, to, Field.addPlayer);
}


// TODO: document
function moveBox(theField, from, to)
{
    _changeField(theField, from, Field.removeBox);
    _changeField(theField, to, Field.addBox);
}






module.exports = 
{
    oppositeDir: oppositeDir,
    move: move,
    isSamePosition: isSamePosition,
    moveBox: moveBox,
    movePlayer: movePlayer

};
