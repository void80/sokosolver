
var ndarray = require("ndarray");
var async = require("async");


// Field contents
var content =
{
    "player": 1,
    "@": 1,
    "playerOnGoal": 2,
    "$": 2,
    "empty": 3,
    " ": 3,
    "wall": 4,
    "#": 4,
    "box": 5,
    "O": 5,
    "boxOnGoal": 6,
    "0": 6,
    "goal": 7,
    ".": 7
};


// TODO: calculate this automatically
var sign = 
{
    1: "@",
    2: "$",
    3: " ",
    4: "#",
    5: "O",
    6: "0",
    7: "."
};


// TODO: document
function hasPlayer(c)
{
    return  c == content["player"] ||
            c == content["playerOnGoal"]
    ;
}

// TODO: document
function _hasBox(c)
{
    return  c == content["box"] ||
            c == content["boxOnGoal"]
    ;
}



// TODO: document
function addBox(c)
{
    if(c == content["empty"])
    {
        return content["box"];
    }
    else if(c == content["goal"])
    {
        return content["boxOnGoal"];
    }
    else
    {
        throw "Cannot add box";
    }
}

// TODO: document
function removeBox(c)
{
    if(c == content["box"])
    {
        return content["empty"];
    }
    else if(c == content["boxOnGoal"])
    {
        return content["goal"];
    }
    else
    {
        throw "Cannot remove box";
    }
}


// TODO: document
function removePlayer(c)
{
    if(c == content["player"])
    {
        return content["empty"];
    }
    else if(c == content["playerOnGoal"])
    {
        return content["goal"];
    }
    else
    {
        throw "Cannot remove player";
    }
}

// TODO: document
function addPlayer(c)
{
    if(c == content["empty"])
    {
        return content["player"];
    }
    else if(c == content["goal"])
    {
        return content["playerOnGoal"];
    }
    else
    {
        throw "Cannot add player";
    }
}

// TODO: document
function canAddPlayer(c)
{
    return  c == content["empty"] ||
            c == content["goal"]
    ;
}


// TODO: document
function canAddBox(c)
{
    return  c == content["empty"] ||
            c == content["goal"]
    ;
}



// TODO: document
function checkField(field, pos, check)
{
    return check(field.get(pos.col, pos.row));
}



function createField(width, height, contents)
{
    /*
    * create a new Field
    * @param width: the width of the field
    * @param height: the height of the field
    * @param contents: [optional] the contents of the field. 
    */ 
    
    if(!contents)
    {
        contents = new Uint8Array(width * height);
        var empty = content["empty"];
        for(var i = 0; i < this.contents.length; i++)
        {
            contents[i] = empty;
        }
    }

    return ndarray(contents, [width, height]);
}



// TODO: document
function clone(field)
{
    return ndarray(new Uint8Array(field.data), field.shape);
}



// TODO: document
function toString(field)
{
    var result = "";
    for(var row = 0; row < field.shape[1]; row++)
    {
        var line = "";

        for(var col = 0; col < field.shape[0]; col++)
        {
            var c = field.get(col, row);
            if(sign.hasOwnProperty(c))
            {
                line += sign[c];
            }
            else
            {
                throw "Unknown element '" + c + "'";
            }
        }

        result += line + "\n";
    }
    return result;
}


// TODO: document
function fromString(s)
{
    var lines = s.split("\n").slice(0, -1);
    
    var height = lines.length;
    var width = lines[0].length;

    var result = ndarray(new Uint8Array(width * height), [width, height]);

    for(var row = 0; row < height; row++)
    {
        var rowData = lines[row];
        if(rowData.length == width)
        {
            for(var col = 0; col < width; col++)
            {
                result.set(col, row, content[rowData[col]]);
            }
        }
        else
        {
            throw "wrong shape";
        }
    }

    return result;
}


function _floodFill(field, row, col, next)
{
    /*
    * flood fill the player position from row/col. call a function afterwards
    * @param field: the field to be filled
    * @param row: the row to start
    * @param col: the col to start
    * @param next: the function(err) to be called when finished
    */

    var functions = [];

    function _isValid(fieldContent)
    {
        return  fieldContent == content["empty"] || 
                fieldContent == content["goal"]
        ;
    }

    function _checkAndAdd(col, row)
    {
        if(_isValid(field.get(col, row)))
        {
            field.set(col, row, addPlayer(field.get(col, row)));
            functions.push
            (
                function(next)
                {
                    _floodFill(field, row, col, next);
                }
            );
        }
    }

    if(row > 0)                     { _checkAndAdd(col    , row - 1); }
    if(row < field.shape[1] - 1)    { _checkAndAdd(col    , row + 1); }
    if(col > 0)                     { _checkAndAdd(col - 1, row    ); }
    if(col < field.shape[0] - 1)    { _checkAndAdd(col + 1, row    ); }
    
    async.parallel
    (
        functions,
        function(err, results)
        {
            next(err);
        }
    );

}


function moveEverywhere(field, next)
{
    /*
    * move the player to every possibly field without moving a box. call a function afterwards
    * @param field: the field
    * @param next: the function(error, result) to be called when finished
    */

    
    var result = clone(field);

    for(var row = 0; row < field.shape[1]; row++)
    {
        for(var col = 0; col < field.shape[0]; col++)
        {
            if(field.get(col, row) == content["player"])
            {
                return _floodFill(result, row, col, function(err)
                {
                    next(err, result);
                });
            }
        }
    }
}

// TODO: document
function isSolved(field)
{
    for(var row = 0; row < field.shape[1]; row++)
    {
        for(var col = 0; col < field.shape[0]; col++)
        {
            if(field.get(col, row) == content["box"])
            {
                return false;
            }
        }
    }

    return true;
}



// TODO: document
function getPlayerPosition(field)
{
    for(var row = 0; row < field.shape[1]; row++)
    {
        for(var col = 0; col < field.shape[0]; col++)
        {
            if(hasPlayer(field.get(col, row)))
            {
                return {row: row, col: col};
            }
        }
    }

    throw "no player found";
}

// TODO: document
function getAllBoxes(field)
{
    var result = [];

    for(var row = 0; row < field.shape[1]; row++)
    {
        for(var col = 0; col < field.shape[0]; col++)
        {
            if(_hasBox(field.get(col, row)))
            {
                result.push({row: row, col: col});
            }
        }
    }

    return result;
}



module.exports = 
{
    createField: createField,
    content: content,
    clone: clone,
    toString: toString,
    fromString: fromString,
    moveEverywhere: moveEverywhere,
    getAllBoxes: getAllBoxes,
    getPlayerPosition: getPlayerPosition,
    canAddPlayer: canAddPlayer,
    hasPlayer: hasPlayer,
    addPlayer: addPlayer,
    removePlayer: removePlayer,
    addBox: addBox,
    removeBox: removeBox,
    canAddBox: canAddBox,
    checkField: checkField,
    isSolved: isSolved
};

