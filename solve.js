
var async = require("async");

var field = require("./field");
var Field = field;
var Helper = require("./helper");


function fromToString()
{
    var fieldString = 
        "#########\n" +
        "#       #\n" +
        "# @ OO  #\n" +
        "#   O   #\n" +
        "#   .   #\n" +
        "# O...O #\n" +
        "#   .   #\n" +
        "#########\n" 
    ;

    var theField = field.fromString(fieldString);

    console.log("-----------------");
    console.log(field.toString(theField));
    console.log("-----------------");
    
    field.moveEverywhere
    (
        theField, 
        function(err, result)
        {
            if(!err)
            {
                console.log(field.toString(result));
                console.log("-----------------");
                console.log(field.toString(theField));
            }
            else
            {
                console.err(err);
            }
        }
    );


    console.log
    (
        JSON.stringify
        (
            field.getAllBoxes(theField)
        )
    );


}



// TODO: document
function _findShortestWay(currentField, pos, next)
{
    var visited = {};
    var best = null;

    var calls = 0;

    function __findShortestWay(currentField, steps, next)
    {
        calls++;
        if(best === null || steps.length < best)
        {
// console.log("1");
            var fieldString = field.toString(currentField);
            if(visited.hasOwnProperty(fieldString))
            {
                if(visited[fieldString] < steps.length)
                {
// console.log("seen before");
                    return next(null, null); // field already visited with few steps
                }
                else
                {
                    // TODO: optimization: store shortest path and use it somehow
                    // But beware: the current shortest path might not be the best
// console.log("seen with more steps");                
                }
            }
// console.log("Check:\n" + fieldString);
            visited[fieldString] = steps.length;

            var currentPosition = field.getPlayerPosition(currentField);
            if(Helper.isSamePosition(currentPosition, pos))
            {
// console.log("Found something");
                best = steps.length;
                return next(null, {field: currentField, steps: steps});
            }
            else
            {
                var functions = [];

                function _checkAndFillFunctions(col, row, dir)
                {
                    if(field.canAddPlayer(currentField.get(col, row)))
                    {
                        var newField = field.clone(currentField);
// console.log("New Field:\n" + field.toString(newField));
                        Helper.movePlayer(newField, currentPosition, {col: col, row: row});
                        var newSteps = steps.concat([dir]);
                        functions.push
                        (
                            function(next)
                            {
                                // use setTimeout to do more of a (first in first called) strategy
                                // seems to be more of a broad then depth search, which is faster for 
                                // this problem
                                setTimeout(function()
                                {
                                    __findShortestWay(newField, newSteps, next);
                                    }, 0);
                            }
                        );
                    }
                }

                var row = currentPosition.row;
                var col = currentPosition.col;

                if(row > 0)                             { _checkAndFillFunctions(col, row - 1, "up"); }
                if(row < currentField.shape[1] - 1)     { _checkAndFillFunctions(col, row + 1, "down"); }
                if(col > 0)                             { _checkAndFillFunctions(col - 1, row, "left"); }
                if(col < currentField.shape[0] - 1)     { _checkAndFillFunctions(col + 1, row, "right"); }

                async.parallel
                (
                    functions,
                    function(err, results)
                    {
                        var best = null;
                        for(var i = 0; i < results.length; i++)
                        {
                            var result = results[i];
                            if(result)
                            {
                                if(best === null || result.steps.length < best.steps.length)
                                {
                                    best = result;
                                }
                            }
                        }
                        next(err, best);
                    }
                );

            }
        }
        else
        {
// console.log("to bad");
            return next(null, null); // already found better result
        }
    }

    return __findShortestWay(field.clone(currentField), [], 
        function(err, result)
        {
// console.log("Done in '" + calls + "' calls");
            if(result)
            {
                next(err, result.field, result.steps);
            }
            else
            {
                next("Found no result", null, null);
            }
        }
    );
}



// TODO: document
function _getMoves(theField, floodedField)
{
    var result = [];

    var allBoxes = field.getAllBoxes(theField);

    for(var i = 0; i < allBoxes.length; i++)
    {
        var boxPosition = allBoxes[i];

        function _checkAndAdd(direction)
        {
            var newPosition = Helper.move(boxPosition, direction);
            var playerHasToBePosition = Helper.move(boxPosition, Helper.oppositeDir[direction]);

            if(
                    field.checkField(theField, newPosition, field.canAddBox)
                &&  Field.checkField(floodedField, playerHasToBePosition, Field.hasPlayer)
                )
            {
                result.push({pos: boxPosition, dir: direction});
            }
        }

        if(boxPosition.row > 0 && boxPosition.row < theField.shape[1] - 1) 
        { 
            _checkAndAdd("down"); 
            _checkAndAdd("up"); 
        }
        if(boxPosition.col > 0 && boxPosition.col < theField.shape[0] - 1)
        { 
            _checkAndAdd("right"); 
            _checkAndAdd("left"); 
        }
    }

    return result;
}



// TODO: document
function _makeMoveAndRecurse(fn, currentField, move, steps)
{
    return function(next)
    {
        _findShortestWay
        (
            currentField, 
            Helper.move(move.pos, Helper.oppositeDir[move.dir]),
            function(err, nextField, additionalSteps)
            {
                if(nextField && additionalSteps)
                {
                    Helper.moveBox(nextField, move.pos, Helper.move(move.pos, move.dir));
                    Helper.movePlayer(nextField, Helper.move(move.pos, Helper.oppositeDir[move.dir]), move.pos);
                    var newSteps = steps.concat(additionalSteps).concat([move.dir]);
                    return fn(nextField, newSteps, next);
                }
                else
                {
console.log("Impossible");
                    return next(null, null); // no way to move box in desired direction
                }
            }
        );
    }
}

// TODO: document
function _findBestResult(candidates)
{
    var result = null;
    if(candidates)
    {
        for(var i = 0; i < candidates.length; i++)
        {
            var candidate = candidates[i];
            if(candidate)
            {
                if(result == null || candidate.length < best.length)
                {
                    result = candidate;
                }
            }
        }
    }
    return result;
}

// TODO: document
function _passOnBestResult(next)
{
    return function(err, results)
    {
// console.log("The results: " + JSON.stringify(results));
        next(err, _findBestResult(results));
    }
}


// TODO: document
function _createMoveFunctions(currentField, filledField, solveFunction, steps)
{
    var moves = _getMoves(currentField, filledField);
    return moves.map
    (
        function(move)
        {
            return _makeMoveAndRecurse(solveFunction, currentField, move, steps)
        }
    );
}

// TODO: document
function solve(startField, next)
{
    var visited = {};
    var best = null;
  
    var pruned = 0;
    var checked = 0;
    var toLong = 0;

    function _solve(currentField, steps, next)
    {
        if(best !== null && steps.length >= best)
        {
            toLong++;
            return next(null, null); // this solution will not be better than a found one
        }

        field.moveEverywhere(currentField, function(err, moveAllField) 
        {
        
        var allFieldString = field.toString(moveAllField);

        if(visited.hasOwnProperty(allFieldString))
        {
            pruned++;
            // just search one solution, not the best one
//            if(visited[allFieldString] < steps.length)
//            {
                return next(null, null); // already seen this field in less steps
//            }
        }
        checked++;
        if(checked % 10 == 0)
        {
            console.log
            (
                "Checked: '" + checked + "', " + 
                "pruned: '" + pruned + "', " + 
                "toLong: '" + toLong + "', " + 
                "ratio: '" + Math.round(pruned / checked * 100) + "%'"
            );
console.log("Check:\n" + Field.toString(currentField));
        }
        visited[allFieldString] = steps.length;

        if(field.isSolved(currentField))
        {
            best = steps.length;
            next(null, steps);
        }
        else
        {
            async.parallel
            (
                _createMoveFunctions(currentField, moveAllField, _solve, steps),
                _passOnBestResult(next)
            );
        }

        }); // of moveEverywhere cb
    }


    return _solve(Field.clone(startField), [], next);

}


// fromToString();
var fieldString = 
    "#########\n" +
    "###   ###\n" +
    "##@ OO ##\n" +
    "### O  ##\n" +
    "#   .   #\n" +
    "# O...O #\n" +
    "####.####\n" +
    "#########\n" 
;
var fieldString = 
    "#########\n" +
    "###   ###\n" +
    "##@ O  ##\n" +
    "###    ##\n" +
    "#   O   #\n" +
    "#   .   #\n" +
    "####.####\n" +
    "#########\n" 
;

var theField = field.fromString(fieldString);
console.log("Searching: \n" + field.toString(theField));
// console.log("Cloned: \n" + field.toString(field.clone(theField)));
/*
 _findShortestWay(theField, {row: 2, col: 2}, function(err, goal, path)
    {
        if(!err)
        {
            console.log("Goal: ");
            console.log(field.toString(goal));
            console.log("Path: ");
            console.log(JSON.stringify(path));
            console.log("That's all, folks!");
        }
        else
        {
            console.log(err);
        }
    }
);
// */

//*
solve(theField, function(err, steps)
{
    if(!err)
    {
        console.log(JSON.stringify(steps));
        console.log("That's all, folks!");
    }
    else
    {
        console.log("Error: " + err);
    }
}
);
//*/





