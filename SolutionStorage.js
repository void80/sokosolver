

var Field = require("./field");
var _ = require("underscore");


// TODO: parametrize next and some functions
function Situation()
{
    this.nextSituation = null;
    this.nextMove = null;
    this.listener = [];
}



// TODO: document
Situation.protoype.getPath = function()
{
    if(this.nextSituation)
    {
        return [this.nextStep].concat(this.nextSituation.getPath());
    }
    else
    {
        return [];
    }
};


Situation.protoype.informAllListeners = function()
{
    _.each(this.listener, this.informListener.bind(this));
};


Situation.protoype.informListener = function(listener)
{
    if(!listener.isInformed)
    {
        listener.isInformed = true;
        listener.listener(this);
    }
};



Situation.protoype.addListener = function(listener)
{
    var listenerObject = {listener: listener, isInformed: false};
    if(this.nextStep !== null)
    {
        this.informListener(listenerObject);
    }

    this.listener.push(listenerObject);
};


// TODO: make to callback storage
function SolutionStorage()
{
    this.situations = {};
}


SolutionStorage.prototype.addSituation = function(field, listener)
{
    var key = Field.toString(field);
    if(Object.prototype.hasOwnProperty.call(this.situations, key))
    {
        var situation = new Situation();
        situation.addListener(listener);
        this.situations[key] = situation;
        return situation;
    }
    else
    {
        this.situations[key].addListener(listener);
        return null;
    }
};


/**
 * find the shortest way to a given position
 * @param field {Field}: the field with player position
 * @param pos {Position}: the position to reach
 * @param callback {Function}: the function(err, path) to be called when finished
 * @returns {null}
 */
function findShortestWay(field, pos, callback)
{
    var visited = {};
    var best = null;

    function _findShortestWay(currentField, steps, next)
    {
        if(best === null || steps.length < best)
        {
            var fieldString = field.toString(currentField);
            if(visited.hasOwnProperty(fieldString))
            {
                if(visited[fieldString] < steps.length)
                {
                    return next(null, null); // field already visited with fewer steps
                }
            }
            visited[fieldString] = steps.length;

            var currentPosition = field.getPlayerPosition(currentField);
            if(Helper.isSamePosition(currentPosition, pos))
            {
                best = steps.length;
                return next(null, {field: currentField, steps: steps});
            }
            else
            {
                var functions = [];

                function _checkAndFillFunctions(col, row, dir)
                {
                    if(field.canAddPlayer(currentField.get(col, row)))
                    {
                        var newField = field.clone(currentField);
                        Helper.movePlayer(newField, currentPosition, {col: col, row: row});
                        var newSteps = steps.concat([dir]);
                        functions.push
                        (
                            function(next)
                            {
                                // use setTimeout to do more of a (first in first called) strategy
                                // seems to be more of a broad then depth search, which is faster for
                                // this problem
                                setTimeout(function()
                                {
                                    __findShortestWay(newField, newSteps, next);
                                    }, 0);
                            }
                        );
                    }
                }

                var row = currentPosition.row;
                var col = currentPosition.col;

                if(row > 0)                             { _checkAndFillFunctions(col, row - 1, "up"); }
                if(row < currentField.shape[1] - 1)     { _checkAndFillFunctions(col, row + 1, "down"); }
                if(col > 0)                             { _checkAndFillFunctions(col - 1, row, "left"); }
                if(col < currentField.shape[0] - 1)     { _checkAndFillFunctions(col + 1, row, "right"); }

                async.parallel
                (
                    functions,
                    function(err, results)
                    {
                        var best = null;
                        for(var i = 0; i < results.length; i++)
                        {
                            var result = results[i];
                            if(result)
                            {
                                if(best === null || result.steps.length < best.steps.length)
                                {
                                    best = result;
                                }
                            }
                        }
                        next(err, best);
                    }
                );

            }
        }
        else
        {
            return next(null, null); // already found better result
        }
    }

    return _findShortestWay(field.clone(currentField), [],
        function(err, result)
        {
            if(result)
            {
                next(err, result.field, result.steps);
            }
            else
            {
                next("Found no result", null, null);
            }
        }
    );
}





module.exports = SolutionStorage;

